WordPress-Load-Balance-Layer-7
===============

Repositori berisi langkah konfigurasi load balancing pada WordPress khususnya pada layer 7

## Topology :

<img src="https://gitlab.com/leonyonz/wordpress-load-balance-layer-7/-/raw/master/template-wp-lb.png" width=480 alt="topology">

**Catatan** : Sistem operasi yang digunakan pada dokumentasi ini adalah CentOS 7, apabila Anda menggunakan sistem operasi selain CentOS 7 maka diperlukan penyesuaian terlebih dahulu pada keseluruh script.

## Persiapan
- 1 Node Load Balance
  - server-ha
- 2 Node Backend Webserver
  - backend1
  - backend2
- 1 Node Database
  - server
- Download repo pada backend1 : `$ git clone https://gitlab.com/leonyonz/wordpress-load-balance-layer-7.git; cd WordPress-Load-Balance-Layer-7;`

## Pre-installation

**1. Buat bash script untuk otomasi install paket epel**

`$ vi epel.sh`
```sh
#!/bin/bash
for i in ip-load-balance \ 
ip-backend1 \
ip-backend2 \
ip-database
do
   ssh root@"$i" yum install epel-release htop yum-utils -y
done
```

Beri permission `execute` pada file `epel.sh` :

`$ chmod +x epel.sh`

Eksekusi file tersebut :

`$ ./epel.sh`

**2. Buat bash script untuk otomasi install paket nginx dan php**

`$ vi ephp.sh`

```sh
#!/bin/bash
for i in ip-backend1 \
ip-backend2
do
   ssh root@"$i" yum install nginx -y;
   ssh root@"$i" "systemctl enable nginx && systemctl start nginx";
   ssh root@"$i" yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y;
   ssh root@"$i" yum-config-manager --enable remi-php72;
   ssh root@"$i" yum -y install php \
			   php-intl \
			   php-common \
			   php-opcache \
			   php-mcrypt \
			   php-cli \
			   php-gd \
			   php-curl \
			   php-mysqlnd  \
			   php-dom \
			   php-curl \
			   php-zip \
			   php-mbstring \
			   php-xml \
			   php-xmlreader \
			   php-simplexml \
			   php-bcmath \
			   php-soap \
			   php-amqplib \
			   php-fpm;
    ssh root@"$i" "systemctl enable php-fpm && systemctl start php-fpm";
done
```

Beri permission `execute` pada file `ephp.sh` :

`$ chmod +x ephp.sh`

Eksekusi file tersebut :

`$ ./ephp.sh`

**3. Install HAProxy pada node load balance**

`$ yum install haproxy -y`

Jalankan service HAProxy

`$ systemctl start haproxy`

Aktifkan service HAProxy

`$ systemctl enable haproxy`

**4. Install MariaDB pada node database**

Tambahkan repository MariaDB :

`$ vi /etc/yum.repos.d/mariadb.repo`

```
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```

Install MariaDB

`$ yum install -y mariadb-server`

Jalankan service MariaDB

`$ systemctl start mariadb`

Aktifkas service MariaDB

`$ systemctl enable mariadb`

## Konfigurasi Node Database

**1. Buat database dengan nama wp_db :**

`$ mysql -u root -p`

`> create database wp_db;`

**2. Beri akses wp_db ke user wp_user**

`$ mysql -u root -p`

`> GRANT ALL PRIVILEGES on wp_db.* to 'wp_user'@'%' IDENTIFIED BY 'some-password';`

`> flush privileges;`

## Konfigurasi Node Webserver1

**1. Konfigurasi Server Block :**

Ubah dan sesuaikan server_name dengan domain Anda :

`$ vi block.sh`

Jika sudah, silakan dieksekusi file berikut ini :

`$ chmod +x block.sh`

`$ ./block.sh`

Verifikasi server block :

`$ ls -lah /etc/nginx/conf.d/blocks.conf`

`$ cat /etc/nginx/conf.d/blocks.conf`

**2. Konfigurasi file php-fpm pada backend1 dan backend2 :**

`$ sed -i 's/apache/nginx/g' /etc/php-fpm.d/www.conf`

Restart service php-fpm :

`$ systemctl restart php-fpm`

**Catatan** : Pastikan dikedua node backend php-fpm telah berjalan dengan baik.

**3. Download file source WordPress :**

`$ sh wordpress.sh`

**4. Atur koneksi database :**

`$ cd /var/www/html/wp;`

`$ cp wp-config-sample.php wp-config.php`

`$ vi wp-config.php`

```
define( 'DB_NAME', 'wp_db' );
define( 'DB_USER', 'wp_user' );
define( 'DB_PASSWORD', 'some-password' );
define( 'DB_HOST', 'server-db.floss.my.id' );
```

**5. Definisikan site_url dan home_url :**

Definisikan site_url dan home_url menggunakan variabel berikut agar website tidak hanya listen ke 1 hostname saja :

`$ vi wp-config.php`

```
define( 'WP_HOME',    'http://' . $_SERVER['HTTP_HOST'] );
define( 'WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] );
```

**Catatan** : Pastikan kembali permission file wp-config.php

**6. Install & konfigurasi lsyncd agar root directory website antar node backend sync :**

Sesuaikan IP Address untuk backend2 :

`$ vi lsyncd.sh`

**Note : Pastikan dari backend1 bisa melakukan remote ssh tanpa password (key-auth) ke backend2**

Apabila sudah yakin, silakan eksekusi file berikut ini :

`$ sh lsyncd.sh`

**Catatan 1** : Pastikan status service lsyncd berjalan dengan baik.

**Catatan 2** : Cek directory /var/www/html/wp pada node backend2, sesuaikan permission dan ownershipnya.

**Catatan 3** : restart service nginx pada node backend2

## Konfigurasi Node HAProxy :

**1. Gunakan konfigurasi berikut ini :**

Sesuaikan terlebih dahulu IP Backend pada file haproxy.sh.

`$ vi haproxy.sh`

Jika sudah yakin, silakan dieksekusi file berikut ini :

`$ sh haproxy.sh`

**Catatan** : Pastikan status service haproxy berjalan dengan baik.

## Pengujian

**1. Pointing sub-domain frontend.floss.my.id ke IP Address node HAProxy**

**2. Buka website http://frontend.floss.my.id**

**3. Lakukan instalasi seperti biasa**

**4. Pastikan response header pada node berubah :**

![Node.png](https://archive.floss.my.id/node.png)

![Node1.png](https://archive.floss.my.id/node1.png)

**5. Instalkan theme custom pada halaman wp-admin.php dan pastikan website tidak error**

## Questions ?
 
If you have questions related to the topic in this repository, please feel free to contact me at :
 
- [**Issue Menu**](https://gitlab.com/leonyonz/wordpress-load-balance-layer-7/issues)
- [**Email**](mailto:yonz@leon36.web.id)
