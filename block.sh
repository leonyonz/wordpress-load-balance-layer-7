#!/bin/bash

echo "server {
        root /var/www/html/wp;
        index index.php index.html index.htm;
        server_name frontend.floss.my.id;
        server_tokens off;
        access_log /var/log/nginx/frontend_access.log;
        error_log /var/log/nginx/frontend_error.log;
        gzip on;
        gzip_vary on;
        gzip_min_length 10240;
        gzip_proxied expired no-cache no-store private auth;
        gzip_types text/plain text/css text/xml text/javascript application/x-javascript application/xml application/javascript text/x-javascript;
        gzip_disable $(echo '"MSIE [1-6]\."');


        location / {
          try_files $(echo "$"uri "$"uri/ /index.php?"$"args);
          proxy_headers_hash_max_size 512;
          proxy_headers_hash_bucket_size 64;
          proxy_set_header Host $(echo "$"host);
          proxy_set_header X-Forwarded-Proto $(echo "$"scheme);
          proxy_set_header X-Real-IP $(echo "$"remote_addr);
          proxy_set_header X-Forwarded-For $(echo "$"proxy_add_x_forwarded_for);
          add_header Front-End-Https on;
        }

    location ~*  \.(jpg|jpeg|png|gif|ico|css|js)$ {
        expires 365d;
    }

    location ~*  \.(pdf)$ {
        expires 30d;
    }
        location ~ \.php$ {
         fastcgi_pass 127.0.0.1:9000;
         fastcgi_index   index.php;
         fastcgi_param SCRIPT_FILENAME $(echo "$"document_root"$"fastcgi_script_name);
         include fastcgi_params;
        }

 }" > /etc/nginx/conf.d/blocks.conf

nginx -t;
nginx -s reload;
systemctl status nginx;
