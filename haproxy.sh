#!/bin/bash

echo "#---------------------------------------------------------------------
# Example configuration for a possible web application.  See the
# full configuration options online.
#
#   http://haproxy.1wt.eu/download/1.4/doc/configuration.txt
#
#---------------------------------------------------------------------

#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    # to have these messages end up in /var/log/haproxy.log you will
    # need to:
    #
    # 1) configure syslog to accept network log events.  This is done
    #    by adding the '-r' option to the SYSLOGD_OPTIONS in
    #    /etc/sysconfig/syslog
    #
    # 2) configure local2 events to go to the /var/log/haproxy.log
    #   file. A line like the following can be added to
    #   /etc/sysconfig/syslog
    #
    #    local2.*                       /var/log/haproxy.log
    #
    log         127.0.0.1 local2

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000

#---------------------------------------------------------------------
# main frontend which proxys to the backends
#---------------------------------------------------------------------

frontend www-http
    bind *:80
    option forwardfor
    option http-server-close
    reqadd X-Forwarded-Proto:\ http
    mode http
    
    #------------------------------------------------------
    # Enable this if you want to use SSL
    # -----------------------------------------------------
    
    #bind *:443 ssl crt /etc/haproxy/cert.pem
    #redirect scheme https code 301 if !{ ssl_fc }
    #reqadd X-Forwarded-Proto:\ https

    acl url_admin  path_beg  -i  /wp-admin /wp-login.php
    acl url_admin hdr(Cookie) core-backend
    use_backend core-backend if url_admin
    http-response set-header X-Server-Node %s
    default_backend dual-backend

backend core-backend
    server mysite1 backend1.floss.my.id:80 check

backend dual-backend
    server mysite1 backend1.floss.my.id:80 check
    server mysite2 backend2.floss.my.id:80 check" > /etc/haproxy/haproxy.cfg;
    
systemctl restart haproxy;
systemctl status haproxy;
echo "";
echo "";

echo "Make sure if HAProxy service is running !";
