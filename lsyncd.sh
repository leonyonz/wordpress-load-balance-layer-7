#!/bin/bash

yum install lsyncd -y;

echo "settings {
        logfile         = $(echo '"/var/log/lsyncd/lsyncd.log"'),
        statusFile      = $(echo '"/tmp/lsyncd.stat"'),
        statusInterval  = 1,
}
sync {
        default.rsync,
        source  = $(echo '"/var/www/html/wp"'),
        target  = $(echo '"103.23.20.132:/var/www/html/wp"'),

rsync = {
        update  = true,
        perms   = true,
        owner   = true,
        group   = true, 
	rsh     = $(echo '"/usr/bin/ssh -l root -i /root/.ssh/id_rsa"'),
}
}
sync {
        default.rsync,
        source  = $(echo '"/etc/nginx/conf.d/"'),
        target  = $(echo '"103.23.20.132:/etc/nginx/conf.d/"'),

rsync = {
        update  = true,
        perms   = true,
        owner   = true,
        group   = true, 
	rsh     = $(echo '"/usr/bin/ssh -l root -i /root/.ssh/id_rsa"'),
}
}" > /etc/lsyncd.conf;

echo "Mencoba melakukan restart service lsycnd !"
echo "";
echo "";
systemctl restart lsyncd;
echo -ne '====>                     (33%)\r'
sleep 1
echo -ne '=======>                  (45%)\r'
sleep 1
echo -ne '===============>          (66%)\r'
sleep 1
echo -ne '=========================>(100%)\r'
echo -ne '\n'
echo "";
echo "";
systemctl status lsyncd > sync-status.log;
echo "";
echo "";
echo "Mencoba merestart webserver dan php pada backend2 !";
echo "";
echo -ne '====>                     (33%)\r'
sleep 1
echo -ne '=======>                  (45%)\r'
sleep 1
echo -ne '===============>          (66%)\r'
sleep 1
echo -ne '=========================>(100%)\r'
echo -ne '\n'
ssh root@103.23.20.132 "nginx -t; nginx -s reload; systemctl restart php-fpm"; clear;
ssh root@103.23.20.132 "systemctl status nginx;" > nginx-status.log
ssh root@103.23.20.132 "systemctl status php-fpm;" > php-status.log
echo "";
echo "";
echo "Status lsyncd dapat dilihat pada file sync-status.log";
echo "Status nginx dapat dilihat pada file nginx-status.log";
echo "Status php-fpm dapat dilihat pada file php-status.log";
echo "";
echo "";
echo "By : https://leon.my.id"
