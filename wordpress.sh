#!/bin/bash

cd /var/www/html/;
wget -c "http://wordpress.org/latest.tar.gz";
tar xvfz latest.tar.gz;
mv wordpress wp
chown -R nginx:nginx /var/www/html/wp;
cd;
cat /dev/null > ~/.bash_history; history -c;
